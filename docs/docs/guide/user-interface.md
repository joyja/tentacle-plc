# User Interface

Tentacle PLC UI is a web interface Tentacle PLC. It interacts directly with the GraphQL API, and allows for basic PLC functionality like starting/stoping/restarting the PLC, monitoring code execution and manipulating variables.

![Tentacle PLC](https://res.cloudinary.com/jarautomation/image/upload/f_auto/v1646799504/Tentacle%20PLC%20Docs/Tentacle%20PLC%20Main.png)

